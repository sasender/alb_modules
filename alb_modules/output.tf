output "application" {
  value = aws_lb.my-aws-alb

}
output "load_balancer_arn" {
  value = aws_lb.my-aws-alb.arn
}
