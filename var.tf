variable "vpc_id" {
  default = null

}

variable "subnet1" {
  default = null
}

variable "subnet2" {
  default = null
}

variable "aws_security_group" {
  default = null
}

variable "instance1_id" {
  default = null
}

variable "instance2_id" {
  default = null
}

variable "security_groups" {
  default = null
}
variable "alb_sg_name" {
  default = null
}
variable "region" {
  default = null
}

variable "name" {
  default = null
}

variable "cidr_blocks" {
  default = null

}

# variable "ports" {
#   type = list(number)
# }

variable "description" {
  default = null
}
variable "security_group_id" {
  default = null
}

variable "vpc_security_group_ids" {
  default = null
}


