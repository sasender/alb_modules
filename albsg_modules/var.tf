variable "vpc_id" {
  default     = null
  description = "(optional) describe your variable"
}

variable "name" {
  default = null
}

# variable "alb_sg_name" {
#   description = "Name for the ALB security group"
#   type        = string
# }

# variable "vpc_id" {
#   description = "VPC ID"
#   type        = string
# }

# variable "http_ingress_from_port" {
#   description = "Ingress rule from port for HTTP"
#   type        = number
#   default     = 80
# }

# variable "http_ingress_to_port" {
#   description = "Ingress rule to port for HTTP"
#   type        = number
#   default     = null
# }

# variable "https_ingress_from_port" {
#   description = "Ingress rule from port for HTTPS"
#   type        = number
#   default     = null
# }

# variable "https_ingress_to_port" {
#   description = "Ingress rule to port for HTTPS"
#   type        = number
#   default     = null
# }

# variable "ingress_cidr_blocks" {
#   description = "CIDR blocks for ingress"
#   type        = list(string)
# }

# variable "alb_sg_name" {

# }

variable "security_group_id" {
  description = "the name of the security group id "
  default     = null
}

variable "protocol" {
  description = "name of the protocal "

}

variable "cidr_blocks" {
  default     = null
  description = "provide the sg blocks"
}

variable "description" {
  description = "the name of the description"

}

variable "ports" {
  description = "provide the ports"

  type = list(number)
}

variable "self" {
  default = null
  type    = bool
}



