resource "aws_security_group" "alb_sg" {
  name                   = var.name
  vpc_id                 = var.vpc_id
  revoke_rules_on_delete = true

  lifecycle {
    create_before_destroy = true
  }


  #     ingress {
  #     from_port   = var.http_ingress_from_port
  #     to_port     = var.http_ingress_to_port
  #     protocol    = "tcp"
  #     cidr_blocks = var.ingress_cidr_blocks
  #   }

  #   ingress {
  #     from_port   = var.https_ingress_from_port
  #     to_port     = var.https_ingress_to_port
  #     protocol    = "tcp"
  #     cidr_blocks = var.ingress_cidr_blocks
  #   }
}





resource "aws_security_group_rule" "sgr_ingress" {
  count       = length(var.ports)
  type        = "ingress"
  from_port   = element(var.ports, count.index)
  to_port     = element(var.ports, count.index)
  protocol    = var.protocol
  cidr_blocks = var.cidr_blocks
  description = var.description

  security_group_id = aws_security_group.alb_sg.id







}


resource "aws_security_group_rule" "sgr_egress" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.alb_sg.id


}
