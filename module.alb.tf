module "alb" {
  source          = "./alb_modules"
  vpc_id          = var.vpc_id
  subnet1         = var.subnet1
  subnet2         = var.subnet2
  instance1_id    = var.instance1_id
  instance2_id    = var.instance2_id
  security_groups = [module.albsg.alb_sg_id]
}







